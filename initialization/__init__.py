"""
@attention: 系统初始化运行时处理逻辑

"""
from flask import Flask

from .init_logger import init_logger
from .init_sqlalchemy import init_db
from .init_redis import init_redis
from .init_blueprint import init_smorest_blueprint
from .init_jwtextend import init_jwt
from .init_request import init_request
from .init_exception import init_exception
from .init_smorest import init_smorest
from .init_schema import init_marshmallow_errorhandler


def init(app: Flask):
    """初始化服务"""
    init_logger(app)
    init_exception(app)
    init_db(app)
    init_redis(app)
    init_smorest(app)
    init_smorest_blueprint(app)
    init_marshmallow_errorhandler(app)
    init_request(app)
    init_jwt(app)
