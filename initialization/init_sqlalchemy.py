#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright DataGrand Tech Inc. All Rights Reserved.
Author: 余熙韩
File: init_sqlalchemy.py
Time: 2023/9/4 17:52
"""
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from flask import Flask

from configs import config

db = SQLAlchemy()
session = db.session

migrate = Migrate(compare_type=True, compare_server_default=True)


# compare_type 此选项配置自动迁移生成子系统以检测列类型更改。
# compare_server_default  迁移默认值


def get_database_uri():
    return 'mysql+pymysql://%s:%s@%s:%s/%s?charset=%s' % (
        config.MYSQL_USER, config.MYSQL_PASSWD,
        config.MYSQL_HOST, config.MYSQL_PORT,
        config.MYSQL_DATABASE, config.MYSQL_CHARSET,
    )


def init_db(app: Flask):
    db_uri = get_database_uri()
    app.logger.debug("MYSQL_CONNECT_URL: " + db_uri)

    app.config["SQLALCHEMY_DATABASE_URI"] = db_uri
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = True
    if config.USE_DB_POOL:
        app.config["SQLALCHEMY_ENGINE_OPTIONS"] = config.SQLALCHEMY_ENGINE_OPTIONS

    db.init_app(app)  # SQLAlchemy 绑定flask
    migrate.init_app(app, db)  # migrate 绑定 flask 和 SQLAlchemy
