#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright DataGrand Tech Inc. All Rights Reserved.
Author: 余熙韩
File: init_blueprint.py
Time: 2023/9/4 19:44
"""
from apps.urls import get_bps
from initialization.init_smorest import get_smorest, init_smorest
from flask import Flask


def init_smorest_blueprint(app: Flask):
    smorest = get_smorest()
    if not smorest:
        init_smorest(app)
        smorest = get_smorest()

    for bp in get_bps():
        smorest.register_blueprint(bp, url_prefix='/api')
