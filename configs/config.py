"""
普通配置,如三方 url， 数据库等
"""
import os
from utils.loader import conf_loader, parse_args

BASEDIR = os.path.dirname(os.path.abspath(os.path.dirname(__file__)))  # 项目目录
PROJECT_PATH = os.path.dirname(BASEDIR)
PROJECT_NAME = BASEDIR.split(os.path.sep)[-1]  # 项目名称

STATIC_URL_PATH = '/hdg/statics'
STATIC_FOLDER = os.path.join(BASEDIR, 'templates/statics')

ENV = os.environ.get(f'{PROJECT_NAME}_ENV', 'LOCAL')  # PROD/UAT/DEV/LOCAL
DBTYPE = "mysql"  # mongo/arangodb/dm/mysql etc
LICENCE_VALIDATE = True
# OCR_URL = r"http://ysocr.datagrand.cn/ysocr"
# IDPS_URL = r"http://idps.datagrand.cn"


#  #### 日志 #####
LOG_DIR = os.path.join(PROJECT_PATH, "logs", "")  # 日志文件夹路径

# ######################################## Gunicorn配置  ########################################
LOG_LEVEL = parse_args("LOG_LEVEL", "INFO")  # 影响Gunicorn和Flask的LOGGER
AUTO_RELOAD = parse_args("AUTO_RELOAD", False, bool)  # 设置此项可以让gunicorn检测文件变化并且重启
WORK_NUMS = parse_args("WORKERS_NUMS", 1, int)  # 如果设置为0则自动设置
BIND = conf_loader("BIND", "0.0.0.0:8000")

# ######################################## Flask-JWT配置  ########################################
SECRET_KEY = conf_loader("SECRET_KEY", "fe216d1bbf1e0d265cde3876758f9130")  # md5 32
SSO_LOGIGN = conf_loader('SSO_LOGIGN', True)  # 是否支持单点登录 True:支持，False:不支持
JWT_ACCESS_TOKEN_EXPIRES = conf_loader('JWT_ACCESS_TOKEN_EXPIRES', 60 * 60 * 24)
LOGIN_KEY = conf_loader('LOGIN_KEY', "lk_")

# ################### mysql 配置 ####################

# 是否使用连接池
USE_DB_POOL = parse_args("USE_DB_POOL", True, bool)

# 连接池配置, 如果USE_DB_POOL为false不生效
SQLALCHEMY_ENGINE_OPTIONS = dict(
    pool_size=parse_args("DB_POOL_SIZE", 5, int),  # 链接池数量
    # max_overflow=100,  # 当连接池中链接已经用完了, 最多还允许建立多少额外的链接
    # pool_timeout=5,   # 指定池的连接超时, 秒为单位
    # 连接池回收时间, 过了n秒之后连接池会释放过期链接并创建新链接, 这个值要小于mysql的max_timeout, 否则会lost connection, 默认8小时
    pool_recycle=parse_args("DB_POOL_RECYCLE", 8 * 60, int),
)


MYSQL_USER = conf_loader('MYSQL_USER', 'root')
MYSQL_PASSWD = conf_loader('MYSQL_PASSWD', 'root')
MYSQL_HOST = conf_loader('MYSQL_HOST', '127.0.0.1')
MYSQL_PORT = conf_loader('MYSQL_PORT', 3306)
MYSQL_DATABASE = conf_loader('MYSQL_DATABASE', "hgd_doc")
MYSQL_CHARSET = conf_loader("MYSQL_CHARSET", "utf8mb4")


# #################### redis 配置 ######################

REDIS_CONFIG = {
    "type": conf_loader('REDIS_TYPE', 'single'),  # single, sentinel, cluster
    "host": conf_loader('REDIS_HOST', '127.0.0.1'),
    "port": conf_loader('REDIS_PORT', 6379),
    "password": conf_loader('REDIS_PASSWORD', ''),
    "username": conf_loader('REDIS_USERNAME', ''),
    "db": conf_loader('REDIS_DB', '1'),
    "decode_responses": True,
}

REDIS_USER_SESSION_PREFIX = "us_"  # users session prefix in redis.
CONF_KEY = "conf_key"  # redis缓存的conf配置key

REDIS_USER_SESSION_TIMEOUT = 7 * 24 * 60 * 60  # users session timeout.
REDIS_CACHING_TIMETOU = 60 * 10
CACHING_KEY_REFIX = "cache_"

USE_REDIS_CACHE = parse_args("USE_REDIS_CACHE", False, bool)
