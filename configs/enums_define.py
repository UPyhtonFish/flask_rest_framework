from utils.enum_tools import Enum


# 枚举定义
class SecretLevel(Enum):
    __enumtag__ = dict(
        public="公开",
        inside="内部",
        secret="秘密",
        confidential="机密",
        supersecret="绝密"
    )

    public = 1  # 公开
    inside = 2  # 内部
    secret = 3  # 秘密
    confidential = 4  # 机密
    supersecret = 5  # 绝密


UserLevel = SecretLevel
