# flask_rest_framework

此项目是结合 flask_smort, marshmallow 搭建自用flask restful 风格框架

## 文件组织说明

```
flask-template-nw
├── apps 
│    ├── app_a                                 # 第一个app，这里的app_a是举例
│    │    ├── tests                            # 当前app的测试代码，生产环境对应的docker中会删除此目录
│    │    │    ├── control_tests.py            # 业务测试代码，开发同学临时测试代码
│    │    │    └── __init__.py
│    │    ├── control.py                       # 业务逻辑代码
│    │    ├── __init__.py     
│    │    ├── scheme.py		   				# 序列化反序列化字段
│    │    ├── model_func.py					# 表查询方法
│    │    ├── model.py
│    │    └── views.py                         # 蓝图->view 定义
│    ├── app_b                                 
│    │    ├── tests
│    │    │    └── __init__.py
│    │    ├── control.py
│    │    ├── __init__.py
│    │    └── views.py
│    ├── common_control.py
│    ├── common_model.py
│    └── __init__.py
├── configs                                # 项目配置文件夹   
│    ├── config.py                         # 常规配置
│    ├── constants.py                      # 常量配置
│    └── __init__.py
├── initialization                         # 系统运行前的初始化动作
│    ├── init_app.py
│    └── __init__.py
├── scheduled_task                         # 定时任务管理
│    ├── config.py
│    ├── __init__.py
│    ├── scheduled.py
│    └── tasks.py
├── tool_for_deploy                                         
│    ├── licence
│    │    ├── generate_machine_code.py
│    │    └── __init__.py
│    ├── script_for_clean
│    │    ├── hotfix_20220601.py
│    │    └── __init__.py
│    ├── script_for_encryption
│    │    ├── encryption.py
│    │    └── __init__.py
│    ├── script_for_init
│    │    ├── init_db.py
│    │    └── __init__.py
│    ├── docker-compose.yml
│    ├── Dockerfile
│    ├── gunicorn_config.py
│    ├── __init__.py
│    └── supervisord.conf
├── utils                                 # 通用工具类集合
│    ├── dauth
│    │    └── __init__.py
│    ├── dbclint
│    │    ├── db_client.py
│    │    └── __init__.py
│    ├── dcache
│    │    ├── __init__.py
│    │    └── redis_service.py
│    ├── dexception
│    │    ├── exception.py
│    │    └── __init__.py
│    ├── dlog
│    │    ├── __init__.py
│    │    └── logger_process.py
│    ├── dqueue
│    │    ├── consumers.py
│    │    ├── __init__.py
│    │    ├── middleware.py
│    │    └── producers.py
│    ├── drequests
│    │    ├── app.py
│    │    └── __init__.py
│    ├── zk
│    │    ├── __init__.py
│    │    └── zookeeper.py
│    └── __init__.py
├── main.py
├── pyproject.toml
├── README.md
└── requirements.txt

```
# 文件引用关系
utils 
config
    --> initialization
          --> apps 
            --> main.py
              --> gunicorn
                --> supervisor

# 启动方式
## 方式一
```
python main.py
```
## 方式二
```
gunicorn -c deploy_tool/gunicorn_config.py main:app
```