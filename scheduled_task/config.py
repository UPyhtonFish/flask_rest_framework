"""
定时任务配置
"""


class Config(object):

    # 类型是threadpool线程池，且设置最大线程数为20
    SCHEDULER_EXECUTORS = {
        'default': {'type': 'threadpool', 'max_workers': 20}
    }

    # 指定是否开启API
    SCHEDULER_API_ENABLED = True

    # 定义时区
    SCHEDULER_TIMEZONE = 'Asia/Shanghai'

    # 配置RESTful API路由的前缀  http://127.0.0.1:5000/datagrand/jobs
    SCHEDULER_API_PREFIX = '/datagrand'

    # 如果需要设定只允许某些主机访问的话，可以设置白名单，如果是允许全部的话，也可以写上*
    SCHEDULER_ALLOWED_HOSTS = ['*']

    # 如果coalesce为True，下次这个任务被执行时，只会执行1次，也就是最后这次，如果为False，那么会执行2次
    # max_instance就是说同一个任务同一时间最多有几个实例在跑
    SCHEDULER_JOB_DEFAULTS = {
        'coalesce': False,
        'max_instances': 3
    }
