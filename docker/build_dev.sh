#!/usr/bin/env bash
set -eux
cd ..
source ./docker/util_function.sh
source ./docker/.conf

tag_suffix=${1-"local"} # default tag is local
output_image_name=${2-0} # default not output

tag="dev_"$tag_suffix

image= "xxx"${DOCKER_NAMESPACE}"/${DOCKER_IMAGE_NAME}:"${tag}
echo ${image}

build_image ${image} ./docker/Dockerfile.dev .

if [[ ${tag_suffix} == 'local' ]];then
    echo 'local image not push to remote, if you need to push, please do is manually'
else
    docker push ${image}
fi

if [[ ${output_image_name} == 1 ]];then
    echo ${image} > ~/dev_image_name.txt
fi
