#!/usr/bin/env bash

build_image(){
    if [[ $# != 3 ]]; then
        echo "Usage : build_image image_name dockerfile context_path"
        exit 0
    fi
    image_name=$1
    dockerfile=$2
    context_path=$3
    docker build -t ${image_name} -f ${dockerfile} ${context_path}
    build_success=$?
    if [[ ${build_success} -eq 0 ]]; then
        echo "Build ### ${image_name} ### success!"
    else
        echo "Build ### ${image_name} ### failed!"
        exit 1
    fi
}

push_image(){
    local_image=$1
    if [[ $2 ]];then
	    remote_image=$2
	    docker tag ${local_image} ${remote_image}
        docker push ${remote_image}
    else
        docker push ${local_image}
    fi
}
