#!/usr/bin/env bash
source ./util_function.sh
source ./.conf

tag_suffix=${1-"local"} # default is dev

sh build_dev.sh ${tag_suffix} 1
test_image=$(cat ~/dev_image_name.txt)
rm ~/dev_image_name.txt

test_container_name="${DOCKER_IMAGE_NAME}_${RANDOM}"

if [[ "$(docker ps -q ${test_container_name} 2> /dev/null)" != "" ]]; then
    echo "container $test_container_name exist, remove it"
    docker stop ${test_container_name} && docker rm ${test_container_name}
fi

echo "run test in container: $test_container_name"
docker run -d --name ${test_container_name} ${test_image}
docker exec ${test_container_name} bash -c "pytest tool_for_test/ "
test_status=$?

docker stop ${test_container_name}
docker rm ${test_container_name}

if [[ ${test_status} -ne 0 ]]; then
    echo "test job fail"
    exit 1
fi
