from flask import Flask

# 蓝图导入
# from apps.private_cloud import urls as private_cloud
# from apps.public_cloud import urls as public_cloud
from configs.config import PROJECT_NAME, STATIC_URL_PATH, STATIC_FOLDER
from initialization import init

# 定时任务功能import began
# from scheduled_task.scheduled import Config, scheduler
# 定时任务功能import end


app = Flask(PROJECT_NAME,
            static_url_path=STATIC_URL_PATH,
            static_folder=STATIC_FOLDER
            )

# 注册
init(app)


# 定时任务功能code began
# 暂时需要自行安装 flask_apscheduler 并启动
# app.config.from_object(Config)
# scheduler.init_app(app)
# scheduler.start()
# 定时任务功能code end


@app.route('/health', methods=['GET'])
def health_check():
    """
    检测系统健康
    过程检测数据库及中间件连接
    """
    return "ok"


# @app.before_request
# def before_request():
#     """请求前相关操作，如验证用户token等操作"""
#     pass
#
#
# @app.after_request
# def after_request(environ):
#     """
#     配合前端设置允许跨域等请求后处理操作
#     """
#     return environ


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000, debug=True)
