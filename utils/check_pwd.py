#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright DataGrand Tech Inc. All Rights Reserved.
Author: 余熙韩
File: check_pwd
Time: 2023/3/16 17:11
"""

import re

def validate_password(password):
    if len(password) < 8 or len(password) > 20:
        return False
    if not re.search(r'\d', password):
        return False
    if not re.search(r'[a-zA-Z]', password):
        return False
    if not re.search(r'[!@#$%^&*(),.?":{}|<>]', password):
        return False
    return True


def camel_to_snake(name):
    import re
    name = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', name).lower()

if __name__ == '__main__':
    # print(validate_password("abcd1234."))
    print(camel_to_snake("issuingDepartment"))
