# coding=utf-8
from flask import request
from .control import user_control, organization_control
from flask.views import MethodView
from utils.response_container import BaseResponse, ListResponse, ListData
from .urls import v1
from .user_schema import (CreateUserParserSchema, UserSchema, UserListParserSchema, LoginParserSchema,
                          LogoutParserSchema, UpdateUserParserSchema, UpdateMyUserParserSchema, UserPwdSchema)
from ..base_schema import BaseResponseSchema, HeaderBaseSchema, ListResponseSchema
from ..common_model import admin_auth, auth, CurrentUser


class LoginView(MethodView):

    @v1.doc(tags=[v1.name], description="登陆", summary="登陆")
    @v1.arguments(LoginParserSchema, as_kwargs=True)
    @v1.response(200, BaseResponseSchema)
    def post(self, **params):
        """登陆"""
        data = user_control.login(**params)
        return BaseResponse(data=data)


class LoginOutView(MethodView):

    @v1.doc(tags=[v1.name], description="登出", summary="登出")
    @v1.arguments(LogoutParserSchema, location="headers")
    @v1.response(200, BaseResponseSchema)
    @auth()
    def post(self, *args):
        """登出"""
        data = user_control.logout()
        return BaseResponse(data=data)


class UserView(MethodView):

    @v1.doc(tags=[v1.name], description="删除用户", summary="删除用户")
    @v1.arguments(HeaderBaseSchema, location='headers')  # HeaderBaseSchema 需要搭配 headers使用
    @v1.response(200, BaseResponseSchema.set_data(UserSchema))
    @admin_auth()
    def get(self, headers: dict, pk: int):
        """获取指定用户信息"""
        data = user_control.get_by_id(pk)
        return BaseResponse(data=data)

    @v1.doc(tags=[v1.name], description="修改用户", summary="修改用户")
    @v1.arguments(HeaderBaseSchema, location='headers')
    @v1.arguments(UpdateUserParserSchema)
    @v1.response(200, BaseResponseSchema.set_data(UserSchema))  # set_data 将对象转为字典
    @admin_auth()
    def put(self, headers: dict, params: dict, pk: int):
        """修改用户信息"""
        data = user_control.update(pk, params)
        return BaseResponse(data=data)

    @v1.doc(tags=[v1.name], description="删除用户", summary="删除用户")
    @v1.arguments(HeaderBaseSchema, location='headers')
    @v1.response(200, BaseResponseSchema)
    @admin_auth()
    def delete(self, headers: dict, pk: int):
        """删除用户"""
        user_control.delete(pk)
        return BaseResponse()


class MyUserView(MethodView):

    @v1.doc(tags=[v1.name], description="获取用户自身信息", summary="获取用户自身信息")
    @v1.arguments(HeaderBaseSchema, location='headers')
    @v1.response(200, BaseResponseSchema)
    @auth()
    def get(self, headers: dict):
        """获取用户自身信息"""
        curr_user = CurrentUser()
        info = curr_user.info()
        return BaseResponse(data=info)

    @v1.doc(tags=[v1.name], description="修改用户自身信息", summary="修改用户自身信息")
    @v1.arguments(HeaderBaseSchema, location='headers')
    @v1.arguments(UpdateMyUserParserSchema)
    @v1.response(200, BaseResponseSchema.set_data(UserSchema))
    @auth()
    def put(self, headers: dict, params: dict):
        """修改用户自身信息"""
        curr_user = CurrentUser()
        data = user_control.update(curr_user.id, params)
        return BaseResponse(data=data)

    @v1.doc(tags=[v1.name], description="检查密码", summary="检查密码")
    @v1.arguments(HeaderBaseSchema, location='headers')
    @v1.arguments(UserPwdSchema)
    @v1.response(200, BaseResponseSchema)
    @auth()
    def post(self, headers: dict, params: dict, pk: int):
        """检查密码"""
        code, msg = user_control.check_pwd(pk, params)
        return BaseResponse(code=code, data=msg)


class UserListView(MethodView):

    @v1.doc(tags=[v1.name], description="创建用户", summary="创建用户")
    @v1.arguments(CreateUserParserSchema, as_kwargs=True)  # as_kwargs=True 表示将Schema作为关键字参数传递，而不是直接将其赋值给函数的参数。
    @v1.response(200, BaseResponseSchema.set_data(UserSchema))  # 这样做的好处是可以避免在调用函数时出现错误，因为函数无法访问未定义的变量名
    def post(self, **requests):
        """注册用户"""
        data = user_control.create(requests)
        return BaseResponse(data=data)

    @v1.doc(tags=[v1.name], description="获取用户列表", summary="获取用户列表")
    @v1.arguments(UserListParserSchema, location="query")
    @v1.response(200, ListResponseSchema.set_item_data(UserSchema))
    def get(self, params: dict):
        """获取用户列表"""
        total, objs = user_control.list(params)
        return ListResponse(
            data=ListData(total=total, items=objs))
