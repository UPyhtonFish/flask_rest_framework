"""
@attention: 业务开发模块
@note: 
test:业务测试
- 做函数级测试，保留测试数据
view:视图
- 接口实现，做数据校验，业务组合，API响应
control:业务实现
- 要求每一个都有对应的业务测试,本身运行依赖最小
"""
from . import urls
