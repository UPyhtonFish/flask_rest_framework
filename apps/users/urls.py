from ..urls import user_bp_v1 as v1  # 导入顺序必须是先导入 ..urls 再导入 .views 否则会出现循环导入
from .views import UserListView, UserView, LoginView, LoginOutView

v1.add_resource(UserListView, '/users')
v1.add_resource(UserView, '/user/<int:pk>')
v1.add_resource(LoginView, '/login')
v1.add_resource(LoginOutView, '/logout')
