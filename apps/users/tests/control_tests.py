# coding=utf-8
"""
Created on 2022年7月10日
@author: hehuafeng
1. 业务测试装饰器自带功能：临界值检测、执行时间
2. 考虑使用自动化生成脚本生成每个control对应的测试代码。
3. 此处测试会验证是否为本地业务环境变量
"""
from configs.config import ENV
from apps.private_cloud.control import control_demo
import time


def control_test(func):
    def wrapper(*args, **kw):
        if ENV != 'LOCAL':
            print(f'业务测试时运行模式请切换为本地配置!')
        else:
            local_time = time.time()
            func(*args, **kw)
            time_info = f'业务代码测试函数 {func.__name__} 运行时间为 {"%.2f" % (time.time() - local_time)}, 参数为{args}'
            print(time_info)

    return wrapper


@control_test
def control_demo_test(bar: str):
    control_demo(bar)


if __name__ == "__main__":
    control_demo_test('测试业务')
