#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright DataGrand Tech Inc. All Rights Reserved.
Author: 余熙韩
File: model_func.py
Time: 2023/9/5 15:20
"""
from apps.base_model_func import BaseModelFunc
from apps.model import UserModel, OrganizationModel
from werkzeug.security import generate_password_hash, check_password_hash
from sqlalchemy import and_, or_


class UserModelFunc(BaseModelFunc[UserModel]):
    """用户相关查表操作"""

    @classmethod
    def generate_password_hash(cls, password: str, salt_length: int = 16):
        """加密密码"""
        return generate_password_hash(password, salt_length=salt_length)

    @classmethod
    def check_password_hash(cls, pwhash: str, password: str):
        """检验密码"""
        return check_password_hash(pwhash, password)

    @classmethod
    def get_by_name(cls, username):
        q = cls._model.query
        data = q.filter(cls._model.username == username).first()
        return data


class OrganizationModelFunc(BaseModelFunc[OrganizationModel]):
    """"""
