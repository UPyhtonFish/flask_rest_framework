# coding=utf-8
"""
control只写业务逻辑，只能被views import
"""

from initialization.init_logger import logger
from .model_func import UserModelFunc, OrganizationModelFunc
from apps.common_control import BaseControl
from utils.exceptions import TipResponse
from utils.check_pwd import validate_password
from apps.common_control import safe_commit
from initialization.init_jwtextend import TokenManager
from configs.config import JWT_ACCESS_TOKEN_EXPIRES
from flask_jwt_extended import current_user


class UserControl(BaseControl):
    _model = UserModelFunc

    def create(self, requests: dict):
        if UserModelFunc.get_by_name(username=requests['username']):
            raise TipResponse("当前用户已存在不可重复创建")
        if not requests['password']:
            requests['password'] = 'abcd@1234'  # 设置默认密码
        if not validate_password(requests['password']):
            raise TipResponse("密码因在8-20位，且满足字母,数字,特殊字符")
        requests['password'] = self._model.generate_password_hash(requests['password'])
        res = super().create(requests)
        return res

    def update(self, pk: int, request: dict):
        if 'password' in request:
            if not validate_password(request['password']):
                raise TipResponse("密码因在8-20位，且满足字母,数字,特殊字符")
            request['password'] = self._model.generate_password_hash(request['password'])
        return super().update(pk, request)



    def login(self, username, password):
        err_msg = "此用户登陆名称错误或密码错误"

        user = UserModelFunc.get_by_name(username=username)
        if not user:
            raise TipResponse(err_msg)

        token_manager = TokenManager()

        if UserModelFunc.check_password_hash(user.password, password):
            token = token_manager.create_access_token(identity=user)
        else:
            raise TipResponse(err_msg)

        first_login = user.first_login
        if first_login:
            with safe_commit():
                user.first_login = False

        return dict(
            id=user.id,
            token=token,
            expires=JWT_ACCESS_TOKEN_EXPIRES,
            is_admin=user.is_admin,
            first_login=first_login
        )

    def logout(self):
        """登出
        """

        # 撤销token
        token_manager = TokenManager()
        token_manager.revoke_token()

        return dict(
            id=current_user.id,
            success=True
        )


class OrganizationControl(BaseControl):
    _model = OrganizationModelFunc


user_control = UserControl()
organization_control = OrganizationControl()
