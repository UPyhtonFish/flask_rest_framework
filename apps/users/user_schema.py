#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright DataGrand Tech Inc. All Rights Reserved.
Author: 余熙韩
File: user_schema.py
Time: 2023/9/5 18:59
"""
from marshmallow import fields, post_dump
from marshmallow.validate import OneOf
from flask_smorest.fields import Upload
from apps.model import UserModel
from .model_func import OrganizationModelFunc

from ..base_schema import BaseSchema, RawBaseSchema, HeaderSchemaMixin
from configs.enums_define import UserLevel


class LoginParserSchema(RawBaseSchema):
    username = fields.Str(required=True)
    password = fields.Str(required=True)


class LogoutParserSchema(RawBaseSchema, HeaderSchemaMixin):
    pass


class CreateUserParserSchema(RawBaseSchema):
    fullname = fields.Str(required=True)
    username = fields.Str(required=True)
    password = fields.Str(required=True)
    user_level = fields.Int(required=True, validate=OneOf(choices=UserLevel.values()))
    is_admin = fields.Bool()
    is_enable = fields.Bool(missing=True)
    organization_id = fields.Int(required=True)


class UserSchema(BaseSchema):
    class Meta(BaseSchema.Meta):
        model = UserModel

    password = fields.Str(load_only=True)

    @post_dump(pass_many=True)
    def dump_organization_id(self, data, many, **kwargs):
        """dump数据后处理
        添加organization信息
        """

        organization_id = data.get('organization_id', 0)

        organization = {
            "name": None
        }
        if organization_id:
            org = OrganizationModelFunc.get_by_id(organization_id)
            if org:
                organization['name'] = org.name

        data['organization'] = organization

        return data


class UserListParserSchema(RawBaseSchema):
    page = fields.Int(missing=1)
    per_page = fields.Int(missing=10)

    organization_id = fields.Int()
    fullname = fields.Str()
    username = fields.Str()
    user_level = fields.Int(validate=OneOf(choices=UserLevel.values()))
    is_admin = fields.Bool()


class UpdateUserParserSchema(RawBaseSchema):
    password = fields.Str()
    user_level = fields.Int(validate=OneOf(choices=UserLevel.values()))
    is_enable = fields.Bool()
    is_admin = fields.Bool()
    organization_id = fields.Int()


class UpdateMyUserParserSchema(RawBaseSchema):

    user_avatar = fields.Str()
    fullname = fields.Str()
    password = fields.Str()


class UserPwdSchema(RawBaseSchema):
    password = fields.Str(required=True)