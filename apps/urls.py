#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright DataGrand Tech Inc. All Rights Reserved.
Author: 余熙韩
File: urls.py
Time: 2023/9/5 19:09
"""
from initialization.init_smorest import Blueprint

BPS = []


def get_bps():
    global BPS
    return BPS


def create_blueprint(*args, **kwargs):
    global BPS
    bp = Blueprint(*args, **kwargs)
    BPS.append(bp)
    return bp


user_bp_v1 = create_blueprint("用户管理[User]", "user_bp_v1")

from . import users

"""
    必须把url函数导入到 蓝图的 作用域，否则url不会注册
    导入必须放在生成蓝图之后，因为每个目录下的urls.py 都会导入生成 蓝图，如果导入放在前面，会导致在urls.py 文件中导入需要的蓝图时，报错
    create_blueprint -> urls -> blueprint  
"""

