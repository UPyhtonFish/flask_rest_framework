#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright DataGrand Tech Inc. All Rights Reserved.
Author: 余熙韩
File: base_model_func.py
Time: 2023/9/5 15:10
"""
from typing import Dict, Generic, Iterable, List, Optional, Set, Tuple, Type, TypeVar, Union
from weakref import WeakValueDictionary

from sqlalchemy import and_, or_
from apps.base_model import BaseModel
from initialization.init_sqlalchemy import session

from initialization.init_logger import logger

# 可变映射，值是对象的弱引用
_entities_models = WeakValueDictionary()

ModelType = TypeVar("ModelType", bound=BaseModel)


class ModelControlMetaClass(type):
    abstract = False

    def __new__(mcs, name, bases, attrs):
        # 判断抽象表
        if attrs.get('abstract'):
            return type.__new__(mcs, name, bases, attrs)

        if not attrs.get("_model"):  # 如果 _model 没有设置，可以去寻找 model[model] 中的值
            orig_bases = attrs.get("__orig_bases__") or []
            base = [b for i in orig_bases for b in i.__args__ if issubclass(b, BaseModel)]
            if len(base) < 1:
                raise AttributeError("class {} should set these class attributes: _model or define model "
                                     "class yourmodel(Basemodel[yourmodel])".format(name))

            elif len(base) > 1:
                raise AttributeError("class {} can only set one model! ".format(name))

            attrs["_model"] = base[0]

        # children = attrs.get("_children")
        # if children:
        #     _children = set()
        #     for child in children:
        #         if len(child.split(".")) != 2:
        #             raise AttributeError("child should be set like Childrenmodel.parent_id")
        #         _children.add(tuple(child.split(".")))
        #     attrs['_children'] = _children

        cls = type.__new__(mcs, name, bases, attrs)
        cls._entities_models = _entities_models

        model = attrs["_model"]

        cls._entities_models[model.__name__] = cls

        return cls


class BaseModelFunc(Generic[ModelType],
                    metaclass=ModelControlMetaClass):  # Generic[modelType] 泛型 的抽象基类，modelType 是 TypeVar 的对象
    """模型基础类"""

    # 如果希望自己定义增强的 Basemodel 可以设置 abstract 为 True
    abstract = True

    _model: Type[ModelType]

    # children
    # _children: Union[List, Set] = []

    @classmethod
    def get_by_id(cls, _id: int, active_only: bool = True) -> Optional[ModelType]:
        """根据id获取model对象

        :param _id: 主键值
        :type _id: int
        :param active_only: 是否包含软删除的记录, defaults to False
        :type active_only: bool, optional
        """

        q = cls._model.query
        if active_only:
            if not hasattr(cls._model, 'active_query'):
                raise RuntimeError("There is no <active_query> object in this model, "
                                   "and the <active_only> attribute cannot be used")
            q = cls._model.active_query

        q = q.filter(cls._model.id == _id)
        return q.first()

    @classmethod
    def get_by_ids(cls, _ids: list, active_only: bool = True) -> Optional[ModelType]:
        """根据id获取model对象

        :param _id: 主键值
        :type _id: list
        :param active_only: 是否包含软删除的记录, defaults to False
        :type active_only: bool, optional
        """

        q = cls._model.query
        if active_only:
            if not hasattr(cls._model, 'active_query'):
                raise RuntimeError("There is no <active_query> object in this model, "
                                   "and the <active_only> attribute cannot be used")
            q = cls._model.active_query

        q = q.filter(cls._model.id.in_(_ids))
        return q.first()

    @classmethod
    def create(cls, **model) -> ModelType:
        """创建一个model
            这个方法应该在不同的model中被重写，应该有更加详细的函数参数。

        :param **model 需要传入的字段
        """
        model = cls._model(**model)
        session.add(model)
        session.flush()
        return model

    @classmethod
    def update(cls, _id: int, **kwargs) -> Optional[ModelType]:
        """更新model, 需要更新的key-value通过不定关键字参数传入

        :param _id: model主键
        :type _id: int
        """
        model = cls._model.query.get(_id)
        if not model:
            return

        model.auto_set_attr(**kwargs)

        session.flush()
        session.refresh(model)
        return model

    @classmethod
    def bulk_create(cls, model_list: List[Dict]) -> List[ModelType]:
        """批量创建model

        :param model_list 需要创建的model信息列表
        """
        instaces_list = [cls._model(**model) for model in model_list]
        session.bulk_save_objects(instaces_list, return_defaults=True)
        session.flush()
        return instaces_list

    @classmethod
    def delete(cls, _id: int, force_delete=False):
        """删除model

        :param _id: model的主键
        :type _id: int
        :param force_delete: 是否硬删除, defaults to False
        :type force_delete: bool, optional
        """

        q = session.query(cls._model).filter(cls._model.id == _id)

        if force_delete:
            q.delete(synchronize_session=False)
        else:
            if not hasattr(cls._model, 'is_deleted'):
                raise RuntimeError("There is no <is_deleted> object in this model, "
                                   "and the <delete> func cannot be used")
            delete_dict = {"is_deleted": True}
            q.update(delete_dict, synchronize_session=False)
        session.flush()
        return True

    @classmethod
    def list(cls, params: dict, active_only: bool = True, order_by: str = "id", order_by_desc: bool = True):
        q = cls._model.query
        if active_only:
            if not hasattr(cls._model, 'active_query'):
                raise RuntimeError("There is no <active_query> object in this model, "
                                   "and the <active_only> attribute cannot be used")
            q = cls._model.active_query  # 判断是否删除
        # 排序字段
        order_column = getattr(cls._model, order_by)
        order_stmt = order_column.desc() if order_by_desc else order_column.asc()
        page = params.pop("page")
        per_page = params.pop("per_page")
        # 获取 and 查询条件
        query_list = cls.get_filter(params)
        q = q.filter(and_(*query_list)).order_by(order_stmt)
        # 分页
        total, objs = cls.query_paginate(q, page=page, per_page=per_page)
        return total, objs

    @classmethod
    def query_paginate(cls, query, page: int = 1, per_page: int = 10):
        """处理分页"""
        pagination = query.paginate(page=page, per_page=per_page)
        objs = pagination.items
        total = pagination.total
        return total, objs

    @classmethod
    def get_filter(cls, params: dict):
        """简单处理了下and 查询 key=value 和 in 查询"""
        query_list = []
        for key, value in params.items():
            # hasattr 用来防止传入参数与model定义参数不一致的情况出现的报错
            if not hasattr(cls._model, key):
                continue
            model_column = getattr(cls._model, key)
            if isinstance(value, str):
                query_list.append(model_column == value)
            elif isinstance(value, (list, set, tuple)):
                query_list.append(model_column.in_(value))

        return query_list
