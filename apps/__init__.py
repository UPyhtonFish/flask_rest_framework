"""
@attention: 应用集合
@author: 白良俊
@note: 
- 该__init__.py文件内禁止编写代码，避免业务测试时的高耦合
- 大部分业务开发人员在此处进行业务开发
- 整体目录结构为
    - 各app
    - 定时任务
    - 业务配置
    - 环境配置
    - 单元测试
    - 启动文件
"""

# 保证定义的模型类能被 flask_migrate 捕获
from .model import ElementalModel, UserModel, DistributeModel, FileModel, FolderModel, OrganizationModel
