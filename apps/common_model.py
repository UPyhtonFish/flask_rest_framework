"""app间通用的model优化至此处"""
from .base_model_func import BaseModelFunc
from functools import partial
from utils.exceptions import TipResponse
from initialization.init_jwtextend import auth as _auth
from initialization.init_jwtextend import CurrentUser as _CurrentUser
from apps.users.model_func import OrganizationModelFunc
from apps.users.control import user_control
from initialization.init_sqlalchemy import session


def check_object_exist(model: BaseModelFunc, _id: int, active_only: bool = True):
    """检查资源是否存在
    """
    instance = model.get_by_id(_id, active_only)
    if not instance:
        raise TipResponse(
            f"此实体 {model} 不存在 <_id: {_id} - active_only: {active_only}>")
    return instance


class CurrentUser(_CurrentUser):

    def info(self):
        """用户个人信息"""
        org_id = self.organization_id
        org_name = ''
        if org_id:
            organization = OrganizationModelFunc.get_by_id(org_id)
            if organization:
                org_name = organization.name

        return dict(fullname=self.fullname,
                    username=self.username,
                    userlevel=self.userlevel,
                    is_admin=self.is_admin,
                    useravatar=self.useravatar,
                    first_login=self.first_login,
                    organization_name=org_name,
                    password=self.password)

    def update_info(self, useravatar: str = None, fullname: str = None):
        """更新个人信息"""
        params = dict()
        if useravatar:
            params['useravatar'] = useravatar
        if fullname:
            params['fullname'] = useravatar
        user_control.update(self.id, params)


def _admin_auth_callback(curr_user):
    if not curr_user.is_admin:
        raise TipResponse('当前用户不是超级管理员')


auth = _auth
admin_auth = partial(_auth, callback=_admin_auth_callback)
