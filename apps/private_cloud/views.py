# coding=utf-8
from flask import jsonify
from apps.private_cloud import control
from flask.views import MethodView


def app_a_demo():
    """
    程序运行后，您可以使用浏览器输入http://127.0.0.1:5000/private_cloud/get 访问此demo
    :return:
    """
    info = []
    try:
        info = control.control_demo("test")
    except Exception as e:
        return jsonify({'data': info, 'err_code': 101, 'msg': f'error: {e}'})

    return jsonify({'data': info, 'err_code': 0, 'msg': 'success!'})


class LoginView(MethodView):

    def post(self):
        """登录接口"""
        pass


class LoginOutView(MethodView):

    def post(self):
        """登出接口"""
        pass


class UserView(MethodView):

    def get(self):
        """获取用户信息"""

    def post(self):
        """注册用户"""

    def put(self):
        """修改用户信息"""




