# coding=utf-8
"""
Created on 2022年7月3日

@author: ljbai
"""
from flask import jsonify
from apps.public_cloud import control


def app_b_demo():
    """
    程序运行后，您可以使用浏览器输入http://127.0.0.1:5000/public_cloud/get 访问此demo
    :return:
    """
    info = []
    try:
        info = control.control_demo()
    except Exception as e:
        return jsonify({'data': info, 'err_code': 101, 'msg': f'error: {e}'})

    return jsonify({'data': info, 'err_code': 0, 'msg': 'success!'})
