# coding=utf-8
"""
control只写业务逻辑，只能被views import
"""
from configs.config import PROJECT_NAME, BASEDIR


def control_demo():
    """
    function demo
    """
    pass

    return {"project_name": PROJECT_NAME, "project_dir": BASEDIR}
