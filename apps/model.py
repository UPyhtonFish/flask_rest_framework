import apps.base_model as models
from sqlalchemy import String, Integer, BigInteger, Boolean, SmallInteger, JSON
from sqlalchemy import Column
from sqlalchemy.dialects.mysql import LONGTEXT

"""
    这里只定义模型类 因为表比较少，我就统一放再一起了
    查询方法放在app下的 model_func.py 中
"""


class UserModel(models.PkBaseModel,
                models.TimeBaseModel,
                models.DescBaseModel,
                models.IsDelBaseModel,
                models.ExtraBaseModel):
    __tablename__ = "user"

    fullname = Column(String(255), nullable=False, comment="用户名称")
    username = Column(String(255), nullable=False, comment="登陆账号")
    password = Column(String(255), nullable=False, comment="用户密码")
    user_level = Column(Integer, nullable=False, comment="用户等级(1公开,2内部,秘密,机密,5绝密)")
    is_admin = Column(Boolean, nullable=False, default=False, comment="超级管理员")
    user_avatar = Column(String(255), comment="用户头像路径")
    is_enable = Column(Boolean, nullable=False, default=True, comment="启用")
    first_login = Column(Boolean, nullable=False, default=True, comment="首次登陆")
    organization_id = Column(Integer, nullable=False, comment="组织id")


class OrganizationModel(models.PkBaseModel,
                        models.TimeBaseModel,
                        models.IsDelBaseModel,
                        models.ExtraBaseModel):
    __tablename__ = "organization"

    name = Column(String(255), nullable=False, comment="组织名称")
    node_id = Column(BigInteger, nullable=False, unique=True, comment="组织节点id")
    parent_node_id = Column(BigInteger, comment="父组织节点id")


class FolderModel(models.PkBaseModel,
                  models.TimeBaseModel,
                  models.IsDelBaseModel,
                  models.ExtraBaseModel):
    __tablename__ = "folder"
    """
        文件夹下是否是文件和文件夹都有，还是只是在最下级文件夹存储文件
        可以在extra_info 里面把目录层级通过json数据保存在最上层文件夹信息中 parent_node_id 值为 None
        {
            name: xxx,
            children ：{
                id: 3,
                name： aaa,
                children : {
                    id: 4, 
                    name : bbb,
                    children : {
                    }
                }
            }
        }

        文件夹所属 权限怎么设计，复杂点还是简单点
    """
    name = Column(String(255), nullable=False, comment="文件夹名称")
    folder_type = Column(SmallInteger, comment="文件夹类型")  # 收文或者发文
    folder_class = Column(SmallInteger, comment="文件夹种类")  # 公有云或者私有云
    secret_level = Column(SmallInteger, comment="密级")
    node_id = Column(BigInteger, nullable=False, unique=True, comment="文件节点id")
    parent_node_id = Column(BigInteger, comment="父节点id")


class FileModel(models.PkBaseModel,
                models.TimeBaseModel,
                models.DescBaseModel,
                models.IsDelBaseModel,
                models.ExtraBaseModel):
    """源材料表"""

    __tablename__ = "file"

    file_id = Column(BigInteger, comment="文件id")  # 不是file 表id信息, 文件id标识
    file_md5 = Column(String(255), comment="文件md5")

    name = Column(String(255), nullable=False, comment="文件名称")
    title = Column(String(255), comment="文章标题")

    issuing_time = Column(Integer, comment="发文时间(时间戳)")
    issuing_department = Column(String(255), comment="发文单位")
    issuing_number = Column(String(255), comment="发文字号")

    editor = Column(String(255), comment="拣报人(编者)")
    secret_level = Column(SmallInteger, comment="密级")
    text = Column(LONGTEXT, comment='数据原文')
    file_status = Column(SmallInteger, comment="文件状态")
    file_info = Column(JSON, comment="文件信息")  # 目录，类型，大小
    # file_address = Column(String(255), comment='文件地址')
    # file_type = Column(SmallInteger, comment="文件类型")  # pptx, word,pdf 等几种
    folder_id = Column(BigInteger, nullable=False, index=True, comment="所属文件夹")


class ElementalModel(models.PkBaseModel,
                     models.TimeBaseModel,
                     models.DescBaseModel,
                     models.IsDelBaseModel,
                     models.ExtraBaseModel):
    """要素信息表"""
    __tablename__ = "elemental"

    file_id = Column(BigInteger, nullable=False, index=True, comment="文件信息对应id")
    title = Column(String(255), comment="文章标题")

    from_department = Column(String(255), comment="来文单位")
    from_code = Column(String(255), comment="来文字号")

    issuing_time = Column(Integer, comment="印发时间(时间戳)")
    issuing_department = Column(String(255), comment="印发单位")

    receive_department = Column(String(255), comment="主送单位")  # 主送单位是一个还是多个，抄送单位是一个还是多个 ,
    shipping_department = Column(String(255), comment="抄送单位")

    language = Column(String(255), comment="文种")
    secret_level = Column(SmallInteger, comment="密级")
    urgency = Column(Integer, comment="紧急程度")

    organizer_department = Column(String(255), comment="承办单位")
    phone = Column(String(20), comment="手机号")

    receipt_number = Column(String(255), comment="收文字号")
    receipt_time = Column(Integer, comment="收文时间(时间戳)")

    registrant = Column(String(255), comment="登记人")
    registration_time = Column(Integer, comment="登记时间(时间戳)")

    number_pages = Column(Integer, comment="文件页数")
    number_file = Column(Integer, comment="文件数量")


class DistributeModel(models.PkBaseModel,
                      models.TimeBaseModel,
                      models.DescBaseModel,
                      models.ExtraBaseModel):
    """分发行为记录"""
    __tablename__ = "distribute"
    file_id = Column(BigInteger, comment="文件信息对应id")
    organization_ids = Column(JSON, comment="组织ids")
    user_ids = Column(JSON, comment="用户ids")
    status = Column(Boolean, comment="发文状态")
